package graph;

import java.util.ArrayList;
import java.util.Collection;

public class Node {

    private Integer index;

    private String letter;

    private boolean visited;

    private Collection<Node> neighbors;

    public Node(Integer index, String letter) {
        this.index = index;
        this.letter = letter;
        this.neighbors = new ArrayList<>();
        this.visited = false;
    }

    public Collection<Node> getNeighbors() {
        return neighbors;
    }

    public String getLetter() {
        return letter;
    }

    public void addNeighbor(Node other) {
        if (!neighbors.contains(other)) {
            this.neighbors.add(other);
        }
        if (!other.getNeighbors().contains(this)) {
            other.getNeighbors().add(this);
        }
    }

    public Integer getIndex() {
        return index;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }
}
