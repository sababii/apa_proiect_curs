package graph;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class Graph {

    private Set<Node> nodeSet;

    public Graph() {
        this.nodeSet = new TreeSet<>(Comparator.comparing(Node::getIndex));
    }

    public void addNode(Node node) {
        this.nodeSet.add(node);
    }

    public Node findNodeWithIndex(Integer index) {
        for (Node n : nodeSet) {
            if (n.getIndex().equals(index)) {
                return n;
            }
        }
        return null;
    }

    public Set<Node> getNodeSet() {
        return nodeSet;
    }
}
