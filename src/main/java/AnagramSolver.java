import controller.MainWindowController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AnagramSolver extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("mainWindow.fxml"));
        Parent root = fxmlLoader.load();
        stage.setTitle("Anagram Solver+");
        stage.setScene(new Scene(root));
        MainWindowController controller = fxmlLoader.getController();
        controller.setStage(stage);
        controller.initialize();
        stage.show();
    }
}
