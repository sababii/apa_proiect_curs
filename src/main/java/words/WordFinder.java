package words;

import graph.Graph;
import graph.Node;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class WordFinder {

    private static final BigDecimal divider = BigDecimal.valueOf(1000000000);

    public static List<String> findWords(Graph graph) {
        List<String> words = new ArrayList<>();
        String prefix = "";
        BigDecimal start = BigDecimal.valueOf(System.nanoTime());
        for (Node node : graph.getNodeSet()) {
            words.addAll(buildWords(node, prefix));
            node.setVisited(false);
        }
        BigDecimal time = BigDecimal.valueOf(System.nanoTime()).subtract(start).divide(divider, 2, RoundingMode.HALF_UP);
        System.out.println("Found " + words.size() + " total words from current graph");
        System.out.println("Time taken: " + time + " seconds");
        return words;
    }

    private static List<String> buildWords(Node node, String prefix) {
        List<String> words = new ArrayList<>();
        prefix += node.getLetter();
        words.add(prefix);
        node.setVisited(true);

        for (Node neighbor : node.getNeighbors()) {
            if (!neighbor.isVisited()) {
                words.addAll(buildWords(neighbor, prefix));
                neighbor.setVisited(false);
            }
        }
        return words;
    }

    public static Set<String> findUniqueWords(Collection<String> words) {
        return new HashSet<>(words);
    }

    public static List<String> findDictionaryWords(Collection<String> words, Dictionary dictionary) throws IOException {
        List<String> dictionaryWords = new ArrayList<>();
        BigDecimal start = BigDecimal.valueOf(System.nanoTime());
        for (String word : words) {
            if (dictionary.isInDictionary(word)) {
                dictionaryWords.add(word);
            }
        }
        BigDecimal time = BigDecimal.valueOf(System.nanoTime()).subtract(start).divide(divider, 2, RoundingMode.HALF_UP);
        System.out.println("Found " + dictionaryWords.size() + " words from current collection in a dictionary of " + dictionary.getSize() + " total words");
        System.out.println("Time taken: " + time + " seconds");
        return dictionaryWords;
    }

}
