package words;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

public class Dictionary {

    private int size = 0;
    private Map<Letter, Set<String>> wordIndex;
    private File dictionaryFile;

    public Dictionary(File dictionaryFile) {
        this.dictionaryFile = dictionaryFile;
        this.wordIndex = readIndexes();
    }

    public int getSize() {
        return size;
    }

    public Map<Letter, Set<String>> getWordIndex() {
        return wordIndex;
    }

    private Map<Letter, Set<String>> readIndexes() {
        Map<Letter, Set<String>> indexes = new TreeMap<>(Comparator.comparing(Letter::name));
        try {
            for (Letter l : Letter.values()) {
                Set<String> wordsForLetter = new HashSet<>();
                indexes.put(l, wordsForLetter);
            }

            List<String> lines = Files.readAllLines(dictionaryFile.toPath());
            size = lines.size();
            for (String line : lines) {
                Letter letter = Letter.valueOf(line.substring(0, 1).toUpperCase());
                indexes.get(letter).add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return indexes;
    }

    public boolean isInDictionary(String word) {
        Letter letter = Letter.valueOf(word.substring(0, 1).toUpperCase());
        Set<String> words = getWordIndex().get(letter);
        return words.contains(word.toLowerCase());
    }
}
