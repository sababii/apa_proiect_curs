package controller;

import graph.Graph;
import graph.Node;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextInputDialog;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import words.Dictionary;
import words.WordFinder;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class MainWindowController {

    @FXML
    private ListView<String> wordListView;
    @FXML
    private ListView<String> adjacencyListView;
    @FXML
    private Label totalWordsLabel;
    @FXML
    private Label uniqueWordsLabel;
    @FXML
    private Label dictionaryWordsLabel;
    private Graph graph;
    private Dictionary dictionary;
    private Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void initialize() {
        graph = new Graph();
//        menuBar.prefWidthProperty().bind(stage.widthProperty());
    }

    public void addNode() {
        TextInputDialog textInputDialog = new TextInputDialog();
        textInputDialog.setTitle("Node Input");
        textInputDialog.setContentText("Please input Node Letter");
        Optional<String> result = textInputDialog.showAndWait();
        if (result.isPresent()) {
            String letter = result.get().trim();
            if (letter.isEmpty()) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Warning");
                alert.setContentText("Field cannot be empty!");
                alert.showAndWait();
            } else {
                Node node = new Node(adjacencyListView.getItems().size(), letter.substring(0, 1).toUpperCase());
                graph.addNode(node);
                addNodeToListView(node);
            }
        }
    }

    public void addEdge() {
        int id = adjacencyListView.getSelectionModel().getSelectedIndex();
        if (id >= 0) {
            Node node = graph.findNodeWithIndex(id);
            if (node != null) {
                TextInputDialog textInputDialog = new TextInputDialog();
                textInputDialog.setTitle("Node Edge Input");
                textInputDialog.setContentText("Please input adjacent node IDs, separated by commas");
                Optional<String> result = textInputDialog.showAndWait();
                if (result.isPresent()) {
                    String idList = result.get();
                    if (!idList.isEmpty()) {
                        String[] nodes = idList.split(",");
                        for (String entry : nodes) {
                            Integer otherId = Integer.valueOf(entry);
                            Node otherNode = graph.findNodeWithIndex(otherId);
                            if (otherNode != null) {
                                node.addNeighbor(otherNode);
                                adjacencyListView.getItems().set(node.getIndex(), formatAsListLine(node));
                                adjacencyListView.getItems().set(otherNode.getIndex(), formatAsListLine(otherNode));
                            }
                        }
                    } else {
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("Warning");
                        alert.setContentText("No indexes input!");
                        alert.showAndWait();
                    }
                }
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("No node selected!");
            alert.showAndWait();
        }
    }

    public void loadDictionary() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select dictionary file");
        fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Text files", "*.txt"));
        File initialDirectory = new File(System.getProperty("user.dir")
                + File.separator + "src"
                + File.separator + "main"
                + File.separator + "resources"
                + File.separator + "includedDictionary");
        if (initialDirectory.exists()) {
            fileChooser.setInitialDirectory(initialDirectory);
        }
        File dictionaryFile = fileChooser.showOpenDialog(stage);
        if (dictionaryFile != null) {
            dictionaryWordsLabel.setText("");
            dictionary = new Dictionary(dictionaryFile);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Operation successful");
            alert.setHeaderText("Dictionary loaded!");
            alert.setContentText("The loaded dictionary contains " + dictionary.getSize() + " words");
            alert.showAndWait();
        }
    }

    public void quit() {
        System.exit(1);
    }

    public void reset() {
        totalWordsLabel.setText("");
        dictionaryWordsLabel.setText("");
        uniqueWordsLabel.setText("");
        adjacencyListView.getItems().clear();
        wordListView.getItems().clear();
        initialize();
    }

    public void compute() throws IOException {
        List<String> words = WordFinder.findWords(graph);
        Set<String> uniqueWords = WordFinder.findUniqueWords(words);
        totalWordsLabel.setText(String.format("%d", words.size()));
        uniqueWordsLabel.setText(String.format("%d", uniqueWords.size()));
        if (dictionary == null) {
            dictionaryWordsLabel.setWrapText(true);
            dictionaryWordsLabel.setText("NO DICTIONARY SET");
        } else {
            List<String> dictionaryWords = WordFinder.findDictionaryWords(uniqueWords, dictionary);
            dictionaryWords.sort(Comparator.comparing(s -> s));
            dictionaryWordsLabel.setText(String.format("%d", dictionaryWords.size()));
            for (String dictionaryWord : dictionaryWords) {
                wordListView.getItems().add(dictionaryWord);
            }
        }
    }

    public void addString() {
        reset();
        TextInputDialog textInputDialog = new TextInputDialog();
        textInputDialog.setTitle("String Input");
        textInputDialog.setContentText("Please input a string");
        Optional<String> result = textInputDialog.showAndWait();
        if (result.isPresent()) {
            String word = result.get().trim();
            if (word.isEmpty()) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Warning");
                alert.setContentText("Field cannot be empty!");
                alert.showAndWait();
            } else {
                String[] letters = word.split("");
                int counter = 0;
                Node currentNode = new Node(counter++, letters[0]);
                Node previousNode;
                graph.addNode(currentNode);

                for (int i = 1; i < letters.length; i++) {
                    previousNode = currentNode;
                    currentNode = new Node(counter++, letters[i]);
                    graph.addNode(currentNode);
                    currentNode.addNeighbor(previousNode);
                }

                for (Node n : graph.getNodeSet()) {
                    addNodeToListView(n);
                }
            }
        }
    }

    public void linkAll() {
        Set<Node> nodes = graph.getNodeSet();
        for (Node n : nodes) {
            for (Node otherNode : nodes) {
                if (!n.equals(otherNode)) {
                    n.addNeighbor(otherNode);
                }
            }
        }
        for (Node n : nodes) {
            addNodeToListView(n);
        }
    }

    private String formatAsListLine(Node node) {
        String line = String.format("%5s", String.format("%d[%s] | ", node.getIndex(), node.getLetter()));
        StringJoiner stringJoiner = new StringJoiner(", ");
        for (Node other : node.getNeighbors()) {
            stringJoiner.add(String.format("%s(%d) ", other.getLetter(), other.getIndex()));
        }
        return line + stringJoiner;
    }

    private void addNodeToListView(Node node) {
        String listItemText = formatAsListLine(node);
        if (adjacencyListView.getItems().size() <= node.getIndex()) {
            adjacencyListView.getItems().add(node.getIndex(), listItemText);
        } else {
            adjacencyListView.getItems().set(node.getIndex(), listItemText);
        }
    }
}
